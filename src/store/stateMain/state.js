export default function () {
  return {
    // SignalR Connection
    connection: {},
    // SignalR Connection Info/Status
    isConnected: false,
  };
}
