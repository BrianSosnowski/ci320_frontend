import { HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import Utils from './commonUtils.js';

export default {
  computed: {
    stateConnection: {
      get() {
        return this.$store.state.stateMain.connection;
      },
      set(obj) {
        this.$store.commit('stateMain/setConnection', obj);
      },
    },
    stateIsConnected: {
      get() {
        return this.$store.state.stateMain.isConnected;
      },
      set(obj) {
        this.$store.commit('stateMain/setIsConnected', obj);
      },
    },
  },
  startConnection(ref, hub) {
    const refUtil = this;
    // Check for existing connection
    if (!refUtil.stateIsConnected) {
      // Connection is not already established - Start a new one
      const newConnection = new HubConnectionBuilder()
        .withUrl(`https://sonoransoftware.com/api/${hub}`)
        .withAutomaticReconnect([0, 5000, 10000, 30000])
        .configureLogging(LogLevel.Information)
        .build();

      // Update our connection object in the VueX Store
      refUtil.stateConnection = newConnection;

      // Connect to the new connection
      console.log('Starting Connection...');
      refUtil.stateConnection.start()
        .then(() => {
          console.log('Connected!');

          // Register Common Websocket Events (Shared or needed for ALL pages)
          this.registerCommonEvents();

          // Take in the reference scope of the page calling this function
          // Tell the calling page to register their websocket events
          ref.registerPageEvents();

          // Update our connection status
          refUtil.stateIsConnected = true;
        })
        .catch((ex) => {
          console.log(ex);
          refUtil.stateIsConnected = false;
        });
    } else {
      // Connection is already established - But is now being called by a new page navigation

      // Take in the reference scope of the page calling this function
      // Tell the calling page to register their websocket events
      ref.registerPageEvents();
    }
  },

  // Common Websocket Events
  registerCommonEvents() {
    this.stateConnection.on('returnAlertMessage', (alertMessage) => {
      // This is a general alert message event
      // alertMessage is formatted I.A.W. SDD-SHR-0010001
      console.log(alertMessage);

      // Call our alert display utility to build the UI alert
      Utils.generateAlertMessage(alertMessage);
    });
  },
};
