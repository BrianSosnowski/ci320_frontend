import { Notify } from 'quasar';

export default {
  generateAlertMessage(alertMessage) {
    // Description:
    //  Build a Quasar Notify object from our alertMessage object
    // TRACING:
    //  SRD-HMI-0100001
    //  SDD-SHR-0010001

    const qAlert = {
      // Message Text
      message: alertMessage.message,
      // Span onto multiple lines as needed
      multiLine: alertMessage.multiLine,
      // Display the timeout progress bar only if a timeout is specified
      progress: alertMessage.timeout !== 0,
      // Defaults to the top
      position: alertMessage.position,
      // Default "Dismiss" option
      actions: [{ label: 'Dismiss', color: 'yellow' }],
    };

    // The TYPE field specifies a default color and icon
    if (alertMessage.type) {
      qAlert.type = alertMessage.type;
    } else {
      qAlert.color = alertMessage.messageColor;
    }

    // Display icon or avatar
    // Icons or Avatars are only shown if the TYPE is not provided
    if (alertMessage.avatar && !alertMessage.type) {
      qAlert.avatar = alertMessage.avatar;
    } else {
      qAlert.icon = alertMessage.icon;
    }

    Notify.create(qAlert);
  },
};
